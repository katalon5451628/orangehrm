public class Person {
    String name; // instance variable

    public Person(String name) {
        this.name = name;
    }

    public void greet() {
        System.out.println("Hello, my name is " + this.name + ".");
    }

    public static void main(String[] args) {
        Person person1 = new Person("Alice");
        person1.greet(); // Output: Hello, my name is Alice.

        Person person2 = new Person("Bob");
        person2.greet(); // Output: Hello, my name is Bob.
    }
}
